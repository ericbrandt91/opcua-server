package org.htwdd.im.server.util;

import java.util.Locale;

import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.nodes.UaDataType;
import com.prosysopc.ua.nodes.UaObjectType;
import com.prosysopc.ua.nodes.UaType;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.nodes.CacheVariable;
import com.prosysopc.ua.server.nodes.UaObjectTypeNode;
import com.prosysopc.ua.stack.builtintypes.LocalizedText;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.core.Identifiers;

public class AddType {

	
	
	private NodeManagerUaNode managerNode;

	public AddType(NodeManagerUaNode managerNode) {
		this.managerNode = managerNode;
	}
	
	public UaObjectType add(UaServer server, String name) {		
		int ns = managerNode.getNamespaceIndex();
		final NodeId myDeviceTypeId = new NodeId(ns, name);
		UaObjectType myDeviceType = new UaObjectTypeNode(managerNode, myDeviceTypeId, name, Locale.GERMAN);
		try {			
			final UaType baseObjectType = server.getNodeManagerRoot().getType(Identifiers.BaseObjectType);
			
			
			final NodeId myLevelTypeId = new NodeId(ns, "MyLevelType");
		
			// My Level Measurement
			final NodeId myLevelId = new NodeId(ns, "MyLevel");
			UaType doubleType = server.getNodeManagerRoot().getType(Identifiers.Double);
			CacheVariable myLevel = new CacheVariable(managerNode, myLevelId, "MyLevel", LocalizedText.NO_LOCALE);
			myLevel.setDataType((UaDataType) doubleType);
	
			myDeviceType.addComponent(myLevel);
			
			managerNode.addNodeAndReference(baseObjectType, myDeviceType, Identifiers.HasSubtype);
			
			
		} catch (StatusException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return myDeviceType;
	}
}
