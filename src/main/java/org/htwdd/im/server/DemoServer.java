package org.htwdd.im.server;

import com.prosysopc.ua.server.UaServer;

public interface DemoServer {
	public UaServer configure();
}
