package org.htwdd.im.server.util;

import java.util.Locale;

import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.nodes.UaObject;
import com.prosysopc.ua.nodes.UaObjectType;
import com.prosysopc.ua.nodes.UaType;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.nodes.UaObjectNode;
import com.prosysopc.ua.server.nodes.UaObjectTypeNode;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.core.Identifiers;
import com.prosysopc.ua.types.opcua.BaseObjectType;

public class AddNode {

	private final NodeManagerUaNode managerNode;
	public AddNode(NodeManagerUaNode managerNode) {
		this.managerNode = managerNode;
	}
	
	
	public UaObjectNode add(UaServer server, String name,BaseObjectType root, UaObjectType type) {		
		int ns = managerNode.getNamespaceIndex();			
		final NodeId myDeviceId = new NodeId(ns, name);
		UaObjectNode myDevice = new UaObjectNode(this.managerNode, myDeviceId, name, Locale.ENGLISH);
		myDevice.setTypeDefinition(type);			
		root.addReference(myDevice, Identifiers.HasComponent, false);	
		
		return myDevice;
	}
	
	
	
	
	
	
	
}
