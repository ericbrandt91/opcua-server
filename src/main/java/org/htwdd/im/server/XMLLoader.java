package org.htwdd.im.server;

import java.io.IOException;
import java.io.InputStream;

import org.xml.sax.SAXException;

import com.prosysopc.ua.ModelException;
import com.prosysopc.ua.server.ServerCodegenModel;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;

public class XMLLoader {

	private final DemoServer basicServer;
	
	public XMLLoader(final DemoServer basicServer) {
		this.basicServer = basicServer;
	}
	
	
	public UaServer ImportXMLModel(String xmlFileName, ServerCodegenModel model) {
		
		UaServer server = basicServer.configure();		
		try {
			server.init();
			InputStream is = server.getClass()
					.getClassLoader().getResourceAsStream(xmlFileName);
			server.registerModel(model);
			server.getAddressSpace().loadModel(is);
			
		} catch (UaServerException | SAXException | IOException | ModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return server;
		
		
	}
	
}
