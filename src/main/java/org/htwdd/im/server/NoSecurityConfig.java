package org.htwdd.im.server;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.Set;

import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.UserTokenPolicies;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.builtintypes.LocalizedText;
import com.prosysopc.ua.stack.core.ApplicationDescription;
import com.prosysopc.ua.stack.core.ApplicationType;
import com.prosysopc.ua.stack.core.UserTokenPolicy;
import com.prosysopc.ua.stack.transport.security.SecurityMode;

public class NoSecurityConfig extends Configuration{	
	
	public SecurityMode securityMode;	
	public UserTokenPolicy userTokenPolicy;
	
	
	public NoSecurityConfig(int port, String serverName, Set inetAddress) {
		super(port, serverName, inetAddress);
		userTokenPolicy = UserTokenPolicies.ANONYMOUS;
		securityMode = SecurityMode.NONE;
	}
	
	
	
	public void initializeApplicationIdentity(UaServer server)
		      throws SecureIdentityException, IOException, UnknownHostException, UaServerException {
		    // Application Description is sent to clients
		    ApplicationDescription appDescription = new ApplicationDescription();
		    appDescription.setApplicationName(new LocalizedText(serverName, Locale.ENGLISH));

		    // The 'localhost' (all lower case) part in the URI is converted to the actual
		    // host name of the computer in which the application is run
		    appDescription.setApplicationUri("urn:localhost:UA:" + serverName);

		    appDescription.setProductUri("urn:prosysopc.com:UA:" + serverName);
		    appDescription.setApplicationType(ApplicationType.Server);
		    

		    // Due to SDK design, we must have a certificate in order to have any endpoints, therefore
		    // creating a certificate please see SampleConsoleServer.initialize for more information.

		    File privateKeyPath = new File("PKI/CA/private");
		    String organization = "Sample Organization";
		    String privateKeyPassword = "opcua";
		    ApplicationIdentity identity = ApplicationIdentity.loadOrCreateCertificate(appDescription, organization,
		        privateKeyPassword, privateKeyPath, true);

		    server.setApplicationIdentity(identity);
		    server.setUserTokenPolicies(userTokenPolicy);
		   
			
			
	}
	

}
