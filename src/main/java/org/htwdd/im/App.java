package org.htwdd.im;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.htwdd.im.server.BasicServer;

import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;	


public class App 
{
	final String topic1 = "tele/tasmota/SENSOR";
    int qos             = 2;
    static String broker       = "tcp://18.193.130.251:1883";
    static String clientId     = "JavaSample";
    static String userName = "";
    static String password = "";
	
    public static void main( String[] args ) throws UaServerException
    {
    	BasicServer bserver = new BasicServer(50000,"LighterServer");
		
    	
		UaServer server = bserver.configure();
		server.init();
		server.start();
		
		
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            final MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
            
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setUserName(userName);
            connOpts.setPassword(password.toCharArray());
            connOpts.setCleanSession(true);
            System.out.println("Connecting to broker: "+broker);
            sampleClient.connect(connOpts);          
           
            
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
    }
}
