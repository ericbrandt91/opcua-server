package org.htwdd.im.server.util;


import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.builtintypes.Variant;
import com.prosysopc.ua.types.opcua.BaseVariableType;
import com.prosysopc.ua.types.opcua.server.BaseVariableTypeNode;

public class AddVariable {

	private final NodeManagerUaNode nodeManager;
	private final NodeId parent;
	public AddVariable(
			final NodeManagerUaNode nodeManager,
			final NodeId parent) {
		this.nodeManager = nodeManager;
		this.parent = parent;
	}


	public <T extends BaseVariableType> void addVariable(String variableName, Object value, NodeId idientifier) {
		int ns = nodeManager.getNamespaceIndex();
		 
		BaseVariableType instance = nodeManager.createInstance(BaseVariableTypeNode.class, variableName);
		instance.setDataTypeId(idientifier);		
		try {
			instance.setValue(new DataValue(new Variant(value)));
		} catch (StatusException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    try {	    	
			nodeManager.addComponent(nodeManager.getNode(parent), instance);
		} catch (StatusException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
}