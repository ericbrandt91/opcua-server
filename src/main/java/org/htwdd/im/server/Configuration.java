package org.htwdd.im.server;
import java.net.InetAddress;
import java.util.Set;

public abstract class Configuration {
	public int port;
	public String serverName;
	public Set<InetAddress> inetAddress;
	
	public Configuration(
			 int port,
			 String serverName,
			 Set inetAddress) {
		this.port = port;
		this.serverName = serverName;
		this.inetAddress = inetAddress;
	}
	
	public Configuration() {}
}

