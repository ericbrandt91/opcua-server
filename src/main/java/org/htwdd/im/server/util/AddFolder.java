package org.htwdd.im.server.util;



import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.nodes.UaObject;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.core.Identifiers;
import com.prosysopc.ua.types.opcua.server.FolderTypeNode;

public class AddFolder{
	
	private FolderTypeNode folderNode;
	private NodeManagerUaNode managerNode;
	public AddFolder(NodeManagerUaNode managerNode) {
		this.managerNode = managerNode;
	}

	public FolderTypeNode addFolder(UaServer uaServer, String name) {		
		 final UaObject objectsFolder = uaServer.getNodeManagerRoot().getObjectsFolder();
		 final NodeId myObjectsFolderId = new NodeId(this.managerNode.getNamespaceIndex(), name);
		 folderNode = managerNode.createInstance(FolderTypeNode.class, name, myObjectsFolderId);
		 UaNode node = null;
		 try {
			node = managerNode.addNodeAndReference(objectsFolder, folderNode, Identifiers.Organizes);
		} catch (StatusException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 return (FolderTypeNode) node;
	}


}
